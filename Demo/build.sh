#!/bin/sh

# -----------------------------------------------------------------------
# JDK location -- edit or remove (if already set) for your environment
# -----------------------------------------------------------------------

JAVA_HOME="/opt/jdk1.5.0_10"

# -----------------------------------------------------------------------
# Folder locations -- may need to be changed for your environment
# -----------------------------------------------------------------------

CLIENT_JAR_PATH="/tmp/filenet/samples/ce"

# -----------------------------------------------------------------------
# Init the output dir, then compile
# -----------------------------------------------------------------------

JACE_JAR="${CLIENT_JAR_PATH}/lib/Jace.jar"

rm -rf obj
mkdir obj

${JAVA_HOME}/bin/javac -classpath $JACE_JAR -d obj src/cesample/*.java

${JAVA_HOME}/bin/jar cf cesample.jar -C obj cesample
