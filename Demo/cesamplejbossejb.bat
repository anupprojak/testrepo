@echo off

setlocal

rem -----------------------------------------------------------------------
rem JRE location -- edit or remove (if already set) for your environment
rem -----------------------------------------------------------------------

set JAVA_HOME=C:\jdk1.5.0_10

rem -----------------------------------------------------------------------
rem Folder locations -- may need to be changed for your environment
rem -----------------------------------------------------------------------

set APP_PATH=C:\temp\filenet\samples\ce\demo
set CLIENT_JAR_PATH=C:\temp\filenet\samples\ce

rem -----------------------------------------------------------------------
rem Set the CLASSPATH and options, then run
rem -----------------------------------------------------------------------

set APP_JAR=%APP_PATH%\cesample.jar
set JACE_JAR=%CLIENT_JAR_PATH%\lib\Jace.jar
set LOG4J_JAR=%CLIENT_JAR_PATH%\lib\log4j.jar
set JBOSS_JAR=%CLIENT_JAR_PATH%\lib\jbossall-client.jar

set CLASSPATH=%APP_JAR%;%JACE_JAR%;%LOG4J_JAR%;%JBOSS_JAR%

set JAAS=-Djava.security.auth.login.config=%CLIENT_JAR_PATH%\config\jaas.conf.JBoss
set NAMING=-Djava.naming.factory.initial=org.jnp.interfaces.NamingContextFactory

%JAVA_HOME%\bin\java -cp "%CLASSPATH%" "%JAAS%" "%NAMING%" cesample.MainFrame
