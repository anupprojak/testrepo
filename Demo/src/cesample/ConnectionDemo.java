package cesample;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.security.auth.Subject;

import org.json.JSONException;
import org.json.JSONObject;




//This is testing Comment..................!!!!!!!!!!







//Hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii


import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.admin.DatabaseStorageArea;
import com.filenet.api.admin.LocalizedString;
import com.filenet.api.admin.PropertyDefinition;
import com.filenet.api.admin.PropertyDefinitionString;
import com.filenet.api.admin.PropertyTemplateString;
import com.filenet.api.admin.StorageArea;
import com.filenet.api.admin.StoragePolicy;
import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.ObjectStoreSet;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.FilteredPropertyType;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentReference;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.Properties;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;

public class ConnectionDemo 
{
	
	
	public static void main(String[] args)
	{
	System.out.println("Dummy testing sop line");
	    // Set connection parameters; substitute for the placeholders.
	    String uri = "http://ecmdemo1:9080/wsi/FNCEWS40MTOM/";
	    String username = "p8admin";
	    String password = "filenet";
	        
	    // Make connection.
	    Connection conn = Factory.Connection.getConnection(uri);
	    Subject subject = UserContext.createSubject(conn, username, password, null);
	    UserContext.get().pushSubject(subject);

	    try
	    {
	       // Get default domain.
	       Domain domain = Factory.Domain.fetchInstance(conn, null, null);
	       System.out.println("Domain: " + domain.get_Name());

	       ObjectStoreSet osColl = domain.get_ObjectStores();

             // Get particular object store.
		      ObjectStore objStore = Factory.ObjectStore.fetchInstance(domain, "ANUPOS", null); 
		    
	     
		      ConnectionDemo cd=new ConnectionDemo();
		 //cd.createDoc(objStore);
		// cd.ChangeTitle(objStore);       
		 //   cd.GetProperty(objStore);
		//cd.AddContent(objStore);
	    //cd.FetchDocument(objStore);
		     // cd.createClass(objStore);
		      
		      cd.customPropertyTemplate(objStore);
		// cd.ChangeClass(objStore);
	    }
        catch(Exception e){
        	
        	e.printStackTrace();
	       }
		      
                      //Document Deletion 
                      /* doc1.delete();
                       doc1.save(RefreshMode.REFRESH);
                      System.out.println("Doc deleted");
                      */ 
 
      }
	
	
	//Creating class
	public void createClass(ObjectStore objStore)
	{
		
		// Fetch selected class definition from the server
		ClassDefinition objClassDef = Factory.ClassDefinition.fetchInstance(objStore, "Document", null);
		   
		System.out.println("Class definition selected: " + objClassDef.get_Name());
		System.out.println("Type the name to assign to the new subclass:");
		
		// Create subclass of the Folder class
		ClassDefinition objClassDefNew = objClassDef.createSubclass();

		// Set up locale
		LocalizedString objLocStr = Factory.LocalizedString.createInstance();
		objLocStr.set_LocalizedText("CustomClass");
		objLocStr.set_LocaleName(objStore.get_LocaleName());
		
		// Create LocalizedStringList collection
		objClassDefNew.set_DisplayNames(Factory.LocalizedString.createList());
		objClassDefNew.get_DisplayNames().add(objLocStr);

		// Save new class definition to the server
		objClassDefNew.save(RefreshMode.REFRESH);            
		System.out.println("New class definition created: " + objClassDefNew.get_Name());       
		System.out.println(objClassDefNew);          

	}

	
	
	//crating property template
	public void  customPropertyTemplate(ObjectStore objstore) 
	{
		
		// Construct property filter to ensure PropertyDefinitions property of CD is returned as evaluated
		
		// Fetch selected class definition from the server
		ClassDefinition objClassDef = Factory.ClassDefinition.fetchInstance(objstore, "CustomClass", null);                       
		                        
		// Create property template for a single-valued string property
		PropertyTemplateString objPropTemplate = Factory.PropertyTemplateString.createInstance(objstore);
		                        
		// Set cardinality of properties that will be created from the property template
		objPropTemplate.set_Cardinality (Cardinality.SINGLE);

		// Set up locale
		LocalizedString locStr = Factory.LocalizedString.createInstance();
		locStr.set_LocalizedText("CustomePropDefinition");
		locStr.set_LocaleName (objstore.get_LocaleName());

		// Create LocalizedString collection
		objPropTemplate.set_DisplayNames (Factory.LocalizedString.createList());
		objPropTemplate.get_DisplayNames().add(locStr);

		// Save new property template to the server
		objPropTemplate.save(RefreshMode.REFRESH);

		
		
		// Create property definition from property template
		PropertyDefinitionString objPropDef = (PropertyDefinitionString)objPropTemplate.createClassProperty();
		                 
		// Get PropertyDefinitions property from the property cache                     
		PropertyDefinitionList objPropDefs = objClassDef.get_PropertyDefinitions(); 

		// Add new property definition to class definition
		objPropDefs.add(objPropDef);
		objClassDef.save(RefreshMode.REFRESH);                                                                          
		                                        
	}
	
	public void createDoc(ObjectStore objStore)
	{
		   Document doc = Factory.Document.createInstance(objStore, ClassNames.DOCUMENT);
		      System.out.println("ANUPOS");        
		      doc.getProperties().putValue("DocumentTitle", "New_Doc");
		      doc.save(RefreshMode.NO_REFRESH);
		       
		      
		  /*      doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
		        doc.save(RefreshMode.NO_REFRESH);*/
		      
		        Folder folder = Factory.Folder.getInstance(objStore, ClassNames.FOLDER, "/Destination"); // id of folder to which you want to store document.
		        ReferentialContainmentRelationship rcr = folder.file(doc, AutoUniqueName.AUTO_UNIQUE, "DemoDOCUMENT",
		                DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
		        rcr.save(RefreshMode.NO_REFRESH);
	       
		        System.out.println("done doc creation");
		
	}
	
	
	
	
	
	
	
	
	
	public void AddContent(ObjectStore objStore)
	{
		
		
		// ************************************ Document Content Addition Code****************************
        
        // Get document.
            Document doc=Factory.Document.getInstance(objStore, ClassNames.DOCUMENT, "/Destination/testimg" );

            // Check out the Document object and save it.
            doc.checkout(com.filenet.api.constants.ReservationType.EXCLUSIVE, null, doc.getClassName(), doc.getProperties());
            doc.save(RefreshMode.REFRESH);

            // Get the Reservation object from the Document object.
            Document reservation = (Document) doc.get_Reservation();

            // Specify internal and external files to be added as content.
            File internalFile = new File("C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg");
            // non-Windows: File internalFile = new File("/tmp/docs/mydoc.txt");
         //   String externalFile = "text.txt";

            // Add content to the Reservation object.
            try {
                // First, add a ContentTransfer object.
                ContentTransfer ctObject = Factory.ContentTransfer.createInstance();
                FileInputStream fileIS = new FileInputStream(internalFile.getAbsolutePath());
                ContentElementList contentList = Factory.ContentTransfer.createList();
                ctObject.setCaptureSource(fileIS);
                // Add ContentTransfer object to list.
                contentList.add(ctObject);
                ctObject.set_ContentType("image/jpeg");

                reservation.set_ContentElements(contentList);
                reservation.save(RefreshMode.REFRESH);
                }
            catch (Exception e)
            {
                System.out.println(e.getMessage() );
            }

            // Check in Reservation object as major version.
            reservation.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
            reservation.save(RefreshMode.REFRESH);
            
            System.out.println("Added Content");

	}
	
	
	
	
	
  public void GetProperty(ObjectStore objStore){
	
	
    
    //Getting document properties using fetch instacne        //IBM CaseFoundation doc
  Document doc1=Factory.Document.fetchInstance(objStore,"{2DD98517-CC3C-40F6-A4C9-BC0598DD9D3E}" ,null);     
         doc1.save(RefreshMode.REFRESH);
 
   //Changing the storage policy of document
       StoragePolicy sp = Factory.StoragePolicy.getInstance(objStore, new Id("{1EAAF460-E75A-4A56-8E1E-54DA62DAC849}") );
         doc1.set_StoragePolicy(sp);
         doc1.save(RefreshMode.REFRESH);
         
      
         
    //Converting to json object JSONObject obj = new JSONObject();
         JSONObject jobj=new JSONObject();
               try {
				jobj.put("Name", doc1.get_Name());
				jobj.put("Size", doc1.get_ContentSize());
				jobj.put("GUID", doc1.get_Id());
				jobj.put("Date", doc1.get_DateCreated());
				jobj.put("Creator", doc1.get_Creator());
				jobj.put("Policy", doc1.get_StoragePolicy().get_DisplayName());
	            jobj.put("Owner", doc1.get_Owner());
	            jobj.put("StorageArea", doc1.get_StorageArea().get_DisplayName());
	            jobj.put("VersionStatus", doc1.get_VersionStatus());
                
	            System.out.println(jobj);
                  
        doc1.set_Owner("psmall");
           doc1.save(RefreshMode.NO_REFRESH);
	
	
	
               } catch (Exception e) {
   				// TODO Auto-generated catch block
   				e.printStackTrace();
   			}
   	    
   	    finally
   	    {
   	       UserContext.get().popSubject();
   	    }
   	    
           
}	
	
	
public void MoveContent(ObjectStore objStore )
{
	try{
	   // Get the storage area where you want to move the document content.				test1
    StorageArea dsa = Factory.StorageArea.fetchInstance(objStore, new Id("{54B1ECD7-D9C1-48A3-8B96-81442BDC7276}"), null );

     // Get the Document object whose content you want to move.
     Document docx = Factory.Document.getInstance(objStore, ClassNames.DOCUMENT, "{2B757BEB-3798-42DB-8AE0-554520AD12D5}" );

     // Move the content and save the Document object.
     docx.moveContent(dsa);
     docx.save(RefreshMode.REFRESH);
     System.out.println("Moved the content");
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
 finally
 {
    UserContext.get().popSubject();
 }
 



}
	

public void ChangeTitle(ObjectStore objStore)

{
	   /*Document title changing code*/    
    
     
      PropertyFilter pf1 = new PropertyFilter();
	        pf1.addIncludeProperty(new FilterElement(null, null, null, "DocumentTitle", null));
	        Document doc5 = Factory.Document.fetchInstance(objStore, "/Destination/Desert.jpg",pf1 );

	        
	        Properties props = doc5.getProperties();
	        
	        props.putValue("DocumentTitle", "ChangedTitle"); 
	        doc5.save(RefreshMode.REFRESH );
	        System.out.println("Updated");
           
                      

}


public void FetchDocument(ObjectStore objStore)
{


	String path="D:\\DestinationFolder";
	Document doc=Factory.Document.fetchInstance(objStore, "/Destination/testDemo", null );
	//CEUtil.writeDocContentToFile(doc, path);
	ContentTransfer ct= (ContentTransfer) doc.get_ContentElements().get(0);
	
	String fileName = ct.get_RetrievalName();
	File f = new File(path,fileName);
	InputStream is = doc.accessContentStream(0);
	int c = 0;
	try 
    {
    	FileOutputStream out = new FileOutputStream(f);
    	c = is.read();
    	while ( c != -1)
    	{
    		out.write(c);
    		c = is.read();
    	}
		is.close();
		out.close();
	} 
	catch (IOException e) 
	{
		e.printStackTrace();
	}
	System.out.println("Fetched Successfull");


}

public void ChangeClass(ObjectStore objStore)
{
	 Document doc1=Factory.Document.fetchInstance(objStore,"/Destination/testimg" ,null);     
	 doc1.changeClass("Employee");
      doc1.save(RefreshMode.REFRESH);
      System.out.println("Done");


}

}

