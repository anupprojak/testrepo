@echo off

setlocal

rem -----------------------------------------------------------------------
rem JDK location -- edit or remove (if already set) for your environment
rem -----------------------------------------------------------------------

set JAVA_HOME=C:\jdk150_04

rem -----------------------------------------------------------------------
rem Folder locations -- may need to be changed for your environment
rem -----------------------------------------------------------------------

set CLIENT_JAR_PATH=C:\temp\filenet\samples\ce

rem -----------------------------------------------------------------------
rem Init the output dir, then compile
rem -----------------------------------------------------------------------

set JACE_JAR=%CLIENT_JAR_PATH%\lib\Jace.jar

rmdir /S /Q obj
mkdir obj

"%JAVA_HOME%\bin\javac" -classpath "%JACE_JAR%" -d obj src/cesample/*.java

"%JAVA_HOME%\bin\jar" cf cesample.jar -C obj cesample
